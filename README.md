# Disclamer

First of all, this script is copied from 
https://github.com/williamokano/magic-card-prices
so, kudos for @williamokano

I have modified the algorithm for my own personal use.

# How to use this tool

Just pass the card name forward, as in:

```$ npm start Pasmar```

If you have a name with space, single quote it:

```$ npm start 'Anjo Serra'```

This script is not prepared to return more than 1 result, so, if
you have a list of cards, create a file within this same 
directory named list.txt and add all names you want, one per 
line. After that, you can run the command:

```$ npm run list```

If you want to store results in some txt file use your shell 
knowledge:

```$ npm run list > my_card_prices.txt```

# How accurate are the results?

Very accurate if you type correctly.

# What's going on?

Since ligamagic don't have a public API, this is a necessary tool
to organize my collection and decide what new cards I'm going
to buy.

This script is very poor. Don't expect too much.

It's important to notice that this operation is VERY similar to
access manually the website and grab the  information yourself 
- this is just a more convenient way - and, as it is, be aware 
that the people from magiccards.info can take providences against 
you.
