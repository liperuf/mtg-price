var request = require('request');
var cheerio = require('cheerio');
var Promise = require('promise');
var requestp = require('../../requestp');

/* Resource for ligamagic */
module.exports = {
  find : function(cardname, proxy) {
    // Add some way to get the card with this resource
    console.log("Looking for " + cardname);

    return new Promise(function(resolve, reject) {
      var request_options = {
        headers: {
          'User-Agent' : 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.155 Safari/537.36',
          'Accept' : 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'
        },
        url: "http://ligamagic.com.br/?view=cards%2Fsearch&card=" + encodeURIComponent(cardname)
      };
      requestp(request_options)
        .then(function(body) {
          
          if(body.indexOf("0-0") !== -1)
            reject("Card not found");

          //read the data
          var $ = cheerio.load(body);
          var title = $(".titulo-card").text();
          var titleen = $(".subtitulo-card").text();
          var price = $(".tabela-interna#cotacao-1").find('.lj.b').text().match(/R\$\s\d*,\d*/g).slice(0,1);
          
          var card = {};
          
          card["pt"] = title;
          card["en"] = titleen;
          card["price"] = price;
          
          // resolve(card);
          console.log(card);
          //resolve("Misterious card info!");
        }, function(err) {
          reject(err); //Cascading promises
        });
    });
  }
}
