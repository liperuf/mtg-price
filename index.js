var argv = require('minimist')(process.argv.slice(2));
var card_finder_factory = require('./custom_modules/card/finder/factory');

var website = 'ligamagic';
var cardname = argv.c || argv.n || argv.card || argv.cardName ||  argv.name || argv._[0];
var finder = card_finder_factory.get(website);
var find_promise = finder.find(cardname);

// find_promise.then(function(card) {
//   console.log({status: 'ok', data:
//     {
//       card: card.title.urldecode(),
//       currencies : card.currencies,
//       sets: card.sets,
//       prices : card.prices,
//       url: card.url
//     }
//   });
// }, function(err) {
//   console.log({status: 'error', data:
//     {
//       msg: err
//     }
//   });
// });